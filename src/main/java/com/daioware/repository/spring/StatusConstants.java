package com.daioware.repository.spring;

public interface StatusConstants {
	char ENABLED='E';
	char DISABLED='D';
	char REMOVED='R';
	
	public static boolean isValid(char c) {
		return c==ENABLED || c==DISABLED || c==REMOVED;
	}
}
