package com.daioware.repository.spring;

import java.time.LocalDate;
import java.sql.Date;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class LocalDateConverter implements AttributeConverter<LocalDate, Date> {
	
    @Override
    public Date convertToDatabaseColumn(LocalDate locDate) {
    	return (locDate == null ? null :java.sql.Date.valueOf(locDate));
    }

    @Override
    public LocalDate convertToEntityAttribute(Date date) {
    	return date == null ? null : date.toLocalDate();
    	//return (date == null ? null : date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
    }
}
