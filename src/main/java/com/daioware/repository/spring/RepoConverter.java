package com.daioware.repository.spring;

import java.util.Iterator;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.daioware.repository.util.SortList;

public class RepoConverter {

	public PageRequest toPageRequest(com.daioware.repository.util.PageRequest p) {
		return new PageRequest(p.getPage(),p.getSize());
	}
	
	public Sort toSort(SortList sorts) {
		Iterator<com.daioware.repository.util.Sort>it=sorts.getSorts().iterator();
		com.daioware.repository.util.Sort aux;
		Sort sort;
		aux=it.next();
		if(it.hasNext()) {
			sort=new Sort(
					aux.getDirection().equals(
							com.daioware.repository.util.Sort.Direction.ASC)?
									Sort.Direction.ASC:Sort.Direction.DESC,aux.getMember());
			while(it.hasNext()) {
				aux=it.next();
				sort.and(new Sort(
						aux.getDirection().equals(
								com.daioware.repository.util.Sort.Direction.ASC)?
										Sort.Direction.ASC:Sort.Direction.DESC,aux.getMember()));
			}
		}
		else {
			sort=null;
		}
		return sort;
	}
	public PageRequest toPageRequest(com.daioware.repository.util.PageRequest p,SortList sorts) {
		Sort sort=toSort(sorts);
		return (sort!=null)?new PageRequest(p.getPage(),p.getSize()):toPageRequest(p);
	}
}
