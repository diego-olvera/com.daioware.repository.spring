package com.daioware.repository.spring;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StatusRepository<T extends ItemWithCollections<Id>,Id extends Serializable> extends GenericRepository<T,Id>{

	@Query("select u from #{#entityName} u where u.id=? and u.status=?")
	T findByIdAndStatus(Id id,char status);
		
	@Query("select u from #{#entityName} u where u.status=?")
	Page<T> findByStatus(char status,Pageable pageable);

	@Modifying(clearAutomatically = true)
	@Query("update #{#entityName} u set u.status=:status where u.id=:id")
	int updateStatus(@Param("id") Id id,@Param("status") char status);

	@Modifying(clearAutomatically = true)
	@Query("update #{#entityName} u set u.status=:status,u.lastUpdateDate=:updateDate where u.id=:id")
	int updateStatus(@Param("id") Id id,@Param("status") char status,@Param("updateDate")LocalDateTime updateDate);

	@Modifying(clearAutomatically = true)
	@Query("update #{#entityName} u set u.status=:status,u.lastUpdateDate=:updateDate where u.id=:id")
	int updateStatus(@Param("id") Id id,@Param("status") char status,@Param("updateDate")Instant updateDate);

}
