package com.daioware.repository.spring;

import java.util.Iterator;
import java.util.function.Function;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public class IterablePage<T> implements Iterable<T>{
	private PageRequest pageRequest;
	private Page<T> page;
	private Function<PageRequest, Page<T>> func;
	
	public IterablePage(PageRequest pageRequest,Function<PageRequest, Page<T>> func) {
		setFunc(func);
		setPageRequest(pageRequest);
	}
	
	public PageRequest getPageRequest() {
		return pageRequest;
	}
	public Function<PageRequest, Page<T>> getFunc() {
		return func;
	}
	public void setPageRequest(PageRequest pageRequest) {
		this.pageRequest = pageRequest;
	}
	public void setFunc(Function<PageRequest, Page<T>> func) {
		this.func = func;
	}

	protected class PageIterator implements Iterator<T>{
		private Iterator<T> iterator;

		public PageIterator() {
			iterator=(page=func.apply(pageRequest)).iterator();
		}
		@Override
		public boolean hasNext() {
			return iterator.hasNext()?true:
				page.isLast()?false:
				(iterator=(page=func.apply(pageRequest=(PageRequest) pageRequest.next())).iterator()).hasNext();
		}
		@Override
		public T next() {
			return iterator.next();
		}
	}
	@Override
	public Iterator<T> iterator() {
		return new PageIterator();
		//Without inner class
		/*return new Iterator<T>() {
			private Iterator<T> iterator;
			@Override
			public boolean hasNext() {
				if(page!=null) {
					if(iterator.hasNext()) {
						return true;
					}
					else if(page.isLast()) {
						return false;
					}
					else {
						pageRequest=(PageRequest) pageRequest.next();
						page=func.apply(pageRequest);
						iterator=page.iterator();
						return true;
					}
				}
				else {
					page=func.apply(pageRequest);
					iterator=page.iterator();
					return iterator.hasNext();
				}
			}

			@Override
			public T next() {
				return iterator.next();
			}
		};
		*/
	}
}
