package com.daioware.repository.util.spring;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.daioware.repository.util.IdItem;

@Repository
public interface GenericRepository<T extends IdItem<Id>,Id extends Serializable> extends JpaRepository<T,Id>,JpaSpecificationExecutor<T>{
	
	default void setCollections(T item) {
		
	}
	default boolean existsById(Id id) {
		return exists(id);
	}
	default <S extends T> S save(S x) {
		return saveAndFlush(x);
	}
	default T insert(T x) {
		return x.getId()!=null && exists(x.getId())?null:save(x);
	}
	default T update(T x) {
		return x.getId()!=null && exists(x.getId())?save(x):null;
	}
}
